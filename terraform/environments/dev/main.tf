provider "google" {
  project = var.project_id
  region  = var.region


  add_terraform_attribution_label               = true
  terraform_attribution_label_addition_strategy = "PROACTIVE"
  default_labels                                = var.default_labels
}

provider "helm" {

  # to avoid writing permissions errors in CI runners
  repository_cache       = "${path.cwd}/.helm"
  repository_config_path = "${path.cwd}/.helm/repositories.yaml"

  kubernetes {
    host                   = module.cluster.host
    token                  = module.cluster.token
    cluster_ca_certificate = module.cluster.cluster_ca_certificate
  }

}

provider "kubectl" {
  load_config_file       = false
  host                   = module.cluster.host
  token                  = module.cluster.token
  cluster_ca_certificate = module.cluster.cluster_ca_certificate
}

module "services" {
  source = "../../modules/services"

  services = var.services
}

module "cluster" {
  depends_on = [module.services]
  source = "../../modules/cluster"

  # region     = var.region
  # project_id = var.project_id

  default_labels = var.default_labels

  keepers = {
    version : 1
  }

}

module "monitoring" {
  depends_on = [
    module.cluster,
    module.services,
  ]
  source = "../../modules/monitoring"


  # host                   = module.cluster.host
  # token                  = module.cluster.token
  # cluster_ca_certificate = module.cluster.cluster_ca_certificate

  keepers = {
    version : 1
  }

  # region     = var.region
  # project_id = var.project_id

  # default_labels = var.default_labels

  gke_sa_email = module.cluster.gke_sa_email
}

terraform {

  required_version = ">= 1.4.6, < 2"

  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
    }
  }
  backend "gcs" {
    bucket = "OVERRIDE_ME"
    prefix = "terraform/state"
  }
}
