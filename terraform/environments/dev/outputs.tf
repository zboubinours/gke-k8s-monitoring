output "host" {
#   depends_on = [google_container_cluster.monitoring]
  value      = module.cluster.host
}

output "token" {
  value =  module.cluster.token
  sensitive = true
}

output "cluster_ca_certificate" {
#   depends_on = [google_container_cluster.monitoring]
  value      = module.cluster.cluster_ca_certificate
  }