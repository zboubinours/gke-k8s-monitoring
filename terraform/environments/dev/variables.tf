variable "region" {
  description = "region"
  type        = string
}

variable "project_id" {
  description = "project_id"
  type        = string
}

variable "default_labels" {
  type        = map(string)
  description = "GCP resources default labels"
}

variable "services" {
  type = set(string)
  description = "GCP Services to enable"
}