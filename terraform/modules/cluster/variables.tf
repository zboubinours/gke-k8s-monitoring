# variable "project_id" {
#   type = string
# }

# variable "region" {
#   type = string
# }

# # used for keepers
variable "keepers" {
  type = map(any)
}

variable "default_labels" {
  type        = map(string)
  description = "GCP resources default labels"
}