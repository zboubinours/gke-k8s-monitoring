terraform {
  required_version = ">= 1.4.6, < 2"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">=5.0.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
  }
}

