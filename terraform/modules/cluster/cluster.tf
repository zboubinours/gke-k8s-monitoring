resource "random_id" "gke_sa" {
  keepers     = var.keepers
  byte_length = 8
}

resource "google_service_account" "gke_sa" {
  account_id   = "gke-${random_id.gke_sa.hex}"
  display_name = "GKE Cluster Service Account"
}

resource "google_container_cluster" "monitoring" {
  name = "monitoring"
  # location = var.region

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  node_config {
    service_account = google_service_account.gke_sa.email
    spot            = true
    resource_labels = var.default_labels

    kubelet_config {
      cpu_cfs_quota  = false
      pod_pids_limit = 0
      cpu_manager_policy = ""
    }

  }

  resource_labels = var.default_labels

  deletion_protection = false
}

resource "google_container_node_pool" "main" {
  name = "main"
  # location   = var.region
  cluster = google_container_cluster.monitoring.name
  # node_count = 1


  node_config {
    spot         = true
    machine_type = "e2-standard-4"
    disk_size_gb = 20

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.gke_sa.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    resource_labels = var.default_labels

    kubelet_config {
      cpu_cfs_quota  = false
      pod_pids_limit = 0
      cpu_manager_policy = ""
    }

  }

  autoscaling {
    total_min_node_count = 0
    total_max_node_count = 20
    location_policy      = "ANY" # because we use spot VMs
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }
}

data "google_client_config" "default" {
}
