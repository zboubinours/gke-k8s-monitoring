output "host" {
#   depends_on = [google_container_cluster.monitoring]
  value      = "https://${google_container_cluster.monitoring.endpoint}"
}

output "token" {
  value = data.google_client_config.default.access_token
  sensitive = true
}

output "cluster_ca_certificate" {
#   depends_on = [google_container_cluster.monitoring]
  value      = base64decode(google_container_cluster.monitoring.master_auth[0].cluster_ca_certificate)
}

output "gke_sa_email" {
 value =  google_service_account.gke_sa.email
}