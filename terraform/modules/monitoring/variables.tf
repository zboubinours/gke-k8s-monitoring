# variable "host" {
#   type = string
# }

# variable "token" {
#   type = string
# }

# variable "cluster_ca_certificate" {
#   type = string
# }

variable "keepers" {
  type = map(any)
}
# variable "project_id" {
#   type = string
# }

# variable "region" {
#   type = string
# }
# variable "default_labels" {
#   type        = map(string)
#   description = "GCP resources default labels"
# }

variable "gke_sa_email" {
  type = string
}

variable "ALLOY_VERSION" {
  type    = string
  # default = "0.5.1" # corresponds to app version "1.2.1"
  default = "0.7.0" # corresponds to app version "1.3.1"
}
