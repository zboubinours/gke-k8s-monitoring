resource "helm_release" "cadvisor" {

  name       = "cadvisor"
  repository = "https://ckotzbauer.github.io/helm-charts"
  chart      = "cadvisor"
  version    = "2.3.3"
  namespace  = "cadvisor"
  create_namespace = true
}