
# TODO install grafana agent CRDs
# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
# https://github.com/grafana/agent/tree/v0.24.0/production/operator/crds

# The only Loki helm chart you need:
# https://grafana.com/blog/2022/12/19/the-only-helm-chart-you-need-for-grafana-loki-is-here/

resource "helm_release" "loki" {
  depends_on = [
    helm_release.grafana_agent_operator,
    google_storage_bucket.loki
  ]

  name       = "loki"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "loki"
  version    = "5.43.5"
  namespace  = "grafana"
  values = [
    "${file("${path.module}/helm_values/loki_values.yaml")}"
  ]
  set {
    name  = "loki.storage.bucketNames.admin"
    value = google_storage_bucket.loki[local.loki_buckets.admin].name
  }
  set {
    name  = "loki.storage.bucketNames.ruler"
    value = google_storage_bucket.loki[local.loki_buckets.ruler].name
  }
  set {
    name  = "loki.storage.bucketNames.chunks"
    value = google_storage_bucket.loki[local.loki_buckets.chunks].name
  }
  create_namespace = true
}

resource "helm_release" "grafana_agent_operator" {
  depends_on = [kubectl_manifest.grafana_agent_operator_crd_manifest]

  description = "Loki dependency."
  name        = "loki-grafana-agent-operator"
  repository  = "https://grafana.github.io/helm-charts"
  chart       = "grafana-agent-operator"
  version     = "0.3.15"
  namespace   = "grafana"
  values = [
  ]
  create_namespace = true

}

# https://github.com/grafana/agent/tree/v0.40.0/operations/agent-static-operator/crds

# INSTALL CRDs

resource "random_id" "bucket_id" {
  keepers     = var.keepers
  byte_length = 8
}
resource "google_storage_bucket" "loki" {
  for_each = local.loki_buckets
  name     = "loki-${each.value}-${random_id.bucket_id.hex}"
  location = "EU"
  force_destroy = true
}

# resource "terraform_data" "cluster" {
#   triggers_replace = {
#     grafana_agent_operator_crds_version : var.grafana_agent_operator_crds_version,

#   }

#   provisioner "local-exec" {
#     command = <<EOT
#         export CRD_VERSION=v0.40.0
#         export GIT_REPOSITORY_URL=https://github.com/grafana/agent
#         pushd /tmp
#         git clone --depth 1 --branch $CRD_VERSION $GIT_REPOSITORY_URL
#         cp agent/operations/agent-static-operator/crds ./templates

#         cat > Chart.yaml <<- EOM
#             name: grafana-agent-operator-crds
#             description: grafana-agent-operator CRDs
#             version: 1.0
#             appVersion: 1.0
#             EOM

#         popd
#         EOT
#   }
# }

# resource "helm_release" "grafana_agent_operator_crds" {

#   depends_on = [terraform_data.cluster]

#   name    = "grafana-agent-operator-crds"
#   chart   = "/tmp"
#   version = "1.0"
# }

data "http" "grafana_agent_operator_crd_manifest_file" {
  for_each = toset(local.grafana_agent_operator_crds)
  url      = each.value
}

resource "kubectl_manifest" "grafana_agent_operator_crd_manifest" {
  for_each  = data.http.grafana_agent_operator_crd_manifest_file
  yaml_body = each.value.body
}

# How to test logs
# https://grafana.com/docs/loki/latest/get-started/
# Use `flog` to generate logs

resource "kubectl_manifest" "flog" {
    yaml_body = "${file("${path.module}/helm_values/flog_pod_manifest.yaml")}"
    override_namespace = helm_release.loki.namespace
    force_new = true
}
