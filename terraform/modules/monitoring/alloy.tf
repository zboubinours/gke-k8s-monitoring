resource "helm_release" "grafana-alloy" {
  name       = "alloy"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "alloy"
  version    = var.ALLOY_VERSION

  namespace  = "alloy"
  create_namespace = true

  // TODO aucun des endpoints ne semblent marcher. HTTP ou gRPC. Loki ou autre. Insupportable.
  values = [
    "${file("${path.module}/helm_values/alloy_values.yaml")}"
  ]

  set {
    name = "alloy.configMap.content"
    value = file("${path.module}/config/config.alloy")
  }

  force_update = true
}
