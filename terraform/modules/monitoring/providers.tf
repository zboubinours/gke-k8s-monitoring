terraform {
  required_version = ">= 1.4.6, < 2"
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }

    google = {
      source  = "hashicorp/google"
      version = ">=5.0.0"
    }
  }
}
