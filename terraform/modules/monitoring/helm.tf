resource "helm_release" "grafana" {
  name             = "grafana"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "grafana"
  version          = "6.58.10"
  namespace        = "grafana"
  create_namespace = true
  values = [
    "${file("${path.module}/helm_values/grafana_values.yaml")}"
  ]
}

# resource "helm_release" "grafana-agent" {
#   name       = "grafana-agent"
#   repository = "https://grafana.github.io/helm-charts"
#   chart      = "grafana-agent"
#   version    = "0.24.0"
#   namespace  = "grafana"
#   values = [
#     "${file("${path.module}/helm_values/grafana_agent_values.yaml")}"
#   ]

#   force_update = true

#   # set {
#   #   name = "agent.configMap.content"
#   #   value = "${file("${path.module}/helm_values/grafana_agent_flow_config.river")}"
#   # }

#   create_namespace = true
# }

resource "helm_release" "tempo" {
  name             = "tempo"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "tempo"
  version          = "1.6.1"
  namespace        = "grafana"
  create_namespace = true
}
