resource "google_storage_bucket" "mimir" {
  for_each = local.mimir_buckets
  name     = "mimir-${each.value}-${random_id.bucket_id.hex}"
  location = "EU"

  force_destroy = true
}

resource "helm_release" "mimir" {
  depends_on = [
    google_storage_bucket.mimir
  ]

  name       = "mimir-distributed"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "mimir-distributed"
  version    = "5.4.1"
  namespace  = "grafana"
  values = [
    "${file("${path.module}/helm_values/mimir_values.yaml")}"
  ]
  set {
    name  = "mimir.structuredConfig.blocks_storage.gcs.bucket_name"
    value = google_storage_bucket.mimir[local.mimir_buckets.blocks].name
  }
  set {
    name  = "mimir.structuredConfig.alertmanager_storage.gcs.bucket_name"
    value = google_storage_bucket.mimir[local.mimir_buckets.alertmanager].name
  }
  set {
    name  = "mimir.structuredConfig.ruler_storage.gcs.bucket_name"
    value = google_storage_bucket.mimir[local.mimir_buckets.ruler].name
  }
  create_namespace = true
}

