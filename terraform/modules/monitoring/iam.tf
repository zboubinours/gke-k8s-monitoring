data "google_project" "project" {
}

# GKE SA must be able to read/write loki buckets
resource "google_project_iam_member" "cloud_storage_admin" {
    project = data.google_project.project.number
  role = "roles/storage.admin"
  member = "serviceAccount:${var.gke_sa_email}"
}