resource "helm_release" "kube_state_metrics" {
  depends_on = [
  ]

  name       = "kube-state-metrics"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-state-metrics"
  version    = "5.27.0"
  namespace  = "kube-state-metrics"
  create_namespace = true
}