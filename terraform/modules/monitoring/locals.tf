locals {

  loki_buckets = {
    chunks = "chunks",
    ruler = "ruler",
    admin = "admin",
  }

  mimir_buckets = {
    blocks = "blocks",
    alertmanager = "alertmanager",
    ruler = "ruler",
  }

  grafana_agent_operator_crds = [
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.coreos.com_podmonitors.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.coreos.com_probes.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.coreos.com_servicemonitors.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.grafana.com_grafanaagents.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.grafana.com_integrations.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.grafana.com_logsinstances.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.grafana.com_metricsinstances.yaml",
    "https://raw.githubusercontent.com/grafana/agent/v0.40.0/operations/agent-static-operator/crds/monitoring.grafana.com_podlogs.yaml",
  ]
}
