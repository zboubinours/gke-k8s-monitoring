Monitoring stack in K8s cluster
===

![](docs/grafana.jpeg)

# Goals

![](./docs/stack_labs_training_observability.png)

* Python basic API
* Add tracing
* Add monitoring
* ephemeral environement using Terraform and GCP


Example project: [Jaeger Tracing Hot Rod](https://github.com/jaegertracing/jaeger/blob/main/examples/hotrod/README.md)

# Mozilla SOPS setup

![](https://camo.githubusercontent.com/4ce356d88383ec0075f390e95fc63f431a58fba061048e716c1f45dbc66dbccd/68747470733a2f2f692e696d6775722e636f6d2f5830544d354e492e676966)

To handle secrets, I'll use [sops](https://github.com/mozilla/sops) with [age encryption](https://github.com/FiloSottile/age).

## `secrets.yaml` file setup

In each terraform environement, I'll create a secrets.yaml encrypted file to store config variables.

```sh
# Create sops files
touch .sops.yaml

# Generate GitLab CI age key (so gitlab runners can read sops files)
age-keygen -o key-ci.txt

# add public keys to .sops.yaml file
cat ~/.age/public_key.txt >> .sops.yaml
cat key-ci.txt >> .sops.yaml

# add GitLab CI private key to GitLab CI variables
cat key-ci.txt | pbcopy

# Create dev secrets file
sops terraform/environments/dev/secrets.yaml

# Making sure sops file uses all keys from .sops.yaml config file
sops updatekeys terraform/environments/dev/secrets.yaml

# Test file with GitLab CI private key
export SOPS_AGE_KEY=$(tail -1 ./key-ci.txt)
sops terraform/environments/dev/secrets.yaml

# All done!
```

---
Associer un container a une identite GCP via k8s SA qui est lié a un role GCP (pour gitlab runner)
https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity

Cloud Run permet d'associer une identité a un container sinon


# Schema Grafana

```mermaid

graph TD;

T[Tempo]
L[Loki]
P[Prometheus]
PT[PromTail]

G[Grafana UI]

GA[Grafana Agent connections pool]

KL[K8s Logs]
Py[App python]

G--data source-->T
G--data source-->L
G--data source-->P

T-->GA
L-->GA
P-->GA

GA-->PT
GA--si possible de se passer de promtail-->KL
GA-->Py

PT-->KL

subgraph Node
    subgraph Pod
    Py
    end
    KL
end


```

# Setting up OTLP endpoints

| Component                    | Endpoint                                                   |
| ---------------------------- | ---------------------------------------------------------- |
| Python API                   | OTLP_EXPORTER_ENDPOINT:  http://grafana-agent.grafana:4317 |
| Grafana Agent Receiver HTTP  | endpoint = "0.0.0.0:4318"                                  |
| Grafana Agent Receiver gRPC  | endpoint = "0.0.0.0:4317"                                  |
| Grafana Agent Exporter Tempo | endpoint = "tempo.grafana:4317"                            |
| Grafana Data Source Tempo    | URL = http://tempo:3100/                                   |
| Tempo gRPC OTLP              | URL = http://localhost:4317/                               |


## Ports tempo
  - name: tempo-prom-metrics
    port: 3100
    protocol: TCP
    targetPort: 3100
  - name: tempo-otlp-http-legacy
    port: 55681
    protocol: TCP
    targetPort: 4318
  - name: grpc-tempo-otlp
    port: 4317
    protocol: TCP
    targetPort: 4317
  - name: tempo-otlp-http
    port: 4318
    protocol: TCP
    targetPort: 4318

```mermaid
graph LR;

P[Python API]
GA[Grafana Agent gRPC receiver]
G[Grafana]
T[Tempo database]

P--OTLP exporter endpoint port 4317-->GA
GA--Forward to gRPC port 4317-->T
G--Datasource tempo prom metrics PORT 3100-->T

```

# Data sources

## Tempo

URL = http://tempo:3100

# Install Grafana CRDs

Grafana Alloy requires agent CRDs.

See [Deploy the Agent Operator Custom Resource Definitions (CRDs)](https://grafana.com/docs/agent/latest/operator/getting-started/#deploy-the-agent-operator-custom-resource-definitions-crds)

# Install mimir 

[Get started with Grafana Mimir using the Helm chart](https://grafana.com/docs/helm-charts/mimir-distributed/latest/get-started-helm-charts/)

## External object storage

[Run Grafana Mimir in production using the Helm chart](https://grafana.com/docs/helm-charts/mimir-distributed/latest/run-production-environment-with-helm/)

Endpoint is `url: https://prometehus.prometheus.svc.cluster.local./api/v1/push`


# Stack Metrics Logs Trace Grafana with Alloy

See https://github.com/grafana/intro-to-mltp/blob/main/images/Introduction%20to%20MLTP%20Arch%20Diagram.png

![](https://github.com/grafana/intro-to-mltp/blob/main/images/Introduction%20to%20MLTP%20Arch%20Diagram.png?raw=true)

# Mimir documentation is incorrect

https://github.com/grafana/mimir/issues/7225#issuecomment-1910853783

Do not use common section

# Grafana Alloy

Setup metrics, logs, traces and OTLP exporters:

https://grafana.com/docs/grafana-cloud/monitor-infrastructure/kubernetes-monitoring/configuration/helm-chart-config/otel-collector/

