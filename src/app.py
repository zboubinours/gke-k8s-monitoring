# These are the necessary import declarations
from random import randint

from flask import Flask

## BEGIN
from opentelemetry import metrics, trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor, ConsoleSpanExporter

# from opentelemetry.sdk.metrics.export import ConsoleMetricExporter
# from opentelemetry.sdk.metrics.export.controller import PushController

metrics.set_meter_provider(MeterProvider())
meter = metrics.get_meter(__name__, True)
# exporter = ConsoleMetricsExporter()
# controller = PushController(meter, exporter, 5)

staging_labels = {"environment": "staging"}

requests_counter = meter.create_counter(
    name="requests",
    description="number of requests",
    unit="requests",
)


### END
import config

# Use Resource.create() instead of constructor directly
resource = Resource.create({"service.name": "rolldice_service"})

trace.set_tracer_provider(TracerProvider(resource=resource))

otlp_exporter = OTLPSpanExporter(endpoint=config.OTLP_EXPORTER_ENDPOINT, insecure=True)

trace.get_tracer_provider().add_span_processor(BatchSpanProcessor(otlp_exporter))

tracer = trace.get_tracer("diceroller.tracer")
# Acquire a meter.
meter = metrics.get_meter("diceroller.meter")

# Now create a counter instrument to make measurements with
roll_counter = meter.create_counter(
    "roll_counter",
    description="The number of rolls by roll value",
)

app = Flask(__name__)


@app.route("/rolldice")
def roll_dice():
    requests_counter.add(25, staging_labels)
    requests_counter.add(20, staging_labels)
    return str(do_roll())


def do_roll():
    with tracer.start_as_current_span("do_roll") as rollspan:
        res = randint(1, 6)
        rollspan.set_attribute("roll.value", res)
        # This adds 1 to the counter for the given roll value
        roll_counter.add(1, {"roll.value": res})
        print(res)
        with tracer.start_as_current_span("do_soft_roll") as softrollspan:
            print("hello world!")
            softrollspan.set_attribute("soft.value", res + 2)
        return res


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
