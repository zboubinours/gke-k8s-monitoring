from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS
from diagrams.aws.database import ElastiCache, RDS
from diagrams.aws.network import ELB
from diagrams.aws.network import Route53
from diagrams.aws.storage import S3
from diagrams.aws.general import User
from diagrams.aws.compute import EC2

from diagrams.programming.language import Python
from diagrams.programming.framework import Flask

from diagrams.onprem.monitoring import PrometheusOperator, Prometheus

from diagrams.onprem.tracing import Jaeger

from diagrams.k8s.others import CRD

from diagrams.gcp.operations import Monitoring







with Diagram("Stack Labs Training Observability", show=False):
    # dns = Route53("dns")
    # lb = ELB("lb")

    with Cluster("GKE Cluster"):
        jaeger = Jaeger("Jaeger")
        jaeger_operator = Jaeger("Jaeger Operator")
        prometheus = Prometheus("Prometheus")
        prometheus_operator = PrometheusOperator("Operator")
        service_monitor = CRD("ServiceMonitor")
        google_log = Monitoring("GCP Logging")

        open_telemetry_lib = Python("OpenTelemetry SDK")
        prometheus_lib = Python("Prometheus SDK")
        logging_lib = Python("Python Logging")

        api = Flask("Python Flask API")

        api >> logging_lib >> google_log
        api >> prometheus_lib >> service_monitor >> prometheus
        api >> open_telemetry_lib >> jaeger
        prometheus_operator >> prometheus
        jaeger_operator >> jaeger

    # with Cluster("GKE"):
    #     svc_group = [ECS("api-python"),
    #                  ECS("api2"),
    #                  ECS("api3")]

    # with Cluster("DB Cluster"):
    #     db_primary = RDS("userdb")
    #     db_primary - [RDS("userdb ro")]

    # with Cluster("Frontend"):
    #     front = S3("Website")

    # memcached = ElastiCache("memcached")

    # end_user = User("End users")

    # dns >> lb >> svc_group
    # svc_group >> db_primary
    # svc_group >> memcached
    # end_user >> front >> dns
